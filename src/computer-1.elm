module Main exposing (Model, Msg(..), init, main, update, view)

import Browser
import Debug
import Html exposing (Html, br, button, div, input, label, span, text, textarea)
import Html.Attributes exposing (class, for, id, size, title, value)
import Html.Events exposing (onClick, onInput)
import List exposing (concat, drop, map, range, take)
import List.Extra exposing (greedyGroupsOf)
import Maybe exposing (andThen)
import String
import Time


main =
    Browser.document { init = init, update = update, view = view, subscriptions = subscriptions }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions ( _, State running _ _, _ ) =
    if running then
        Time.every 500 (\_ -> Step)

    else
        Sub.none



-- MODEL


type alias Memory =
    List Int


type PC
    = PC Int


type Acc
    = Acc Int Bool


type alias Running =
    Bool


type alias Input =
    List String


type alias Output =
    List Int


type State
    = State Running Input Output


type alias Registers =
    ( PC, Acc )


type alias Model =
    ( Registers, State, Memory )


init : Int -> ( Model, Cmd msg )
init _ =
    let
        initialMemory =
            [ 1, 398, 1, 399, 298, 4, 508, 502, 499, 2, 502 ]
    in
    ( ( ( PC 0, Acc 0 False ), State False [ "10" ] [], initialMemory ++ (map (\_ -> 0) <| range 0 <| 99 - List.length initialMemory) )
    , Cmd.none
    )



-- UPDATE


type Msg
    = SetCell Int Int
    | SetAcc Int
    | SetPC Int
    | Step
    | Start
    | Stop
    | ClearOutput
    | SetInput String


type Instruction
    = Halt
    | Input
    | Output
    | BranchIfZero
    | BranchIfNegative
    | Add Int
    | Subtract Int
    | Store Int
    | Load Int
    | Jump Int


update : Msg -> Model -> ( Model, Cmd Msg )
update msg (( ( PC pc, Acc acc negative ) as regs, state, memory ) as model) =
    case msg of
        SetCell cellNum newVal ->
            ( ( regs, state, take cellNum memory ++ newVal :: drop (cellNum + 1) memory ), Cmd.none )

        SetAcc newAcc ->
            ( ( ( PC pc, Acc newAcc False ), state, memory ), Cmd.none )

        SetPC newPC ->
            ( ( ( PC newPC, Acc acc negative ), state, memory ), Cmd.none )

        Step ->
            let
                cellValue =
                    Maybe.withDefault 0 <| List.Extra.getAt pc memory

                instruction =
                    Debug.log "instruction" <| parseInstruction cellValue
            in
            ( step model instruction, Cmd.none )

        SetInput v ->
            let
                inputList =
                    String.split " " v
            in
            updateState (\(State r i o) -> State r inputList o) model

        ClearOutput ->
            updateState (\(State r i o) -> State r i []) model

        Start ->
            updateState (\(State _ i o) -> State True i o) model

        Stop ->
            updateState (\(State _ i o) -> State False i o) model


updateState : (State -> State) -> Model -> ( Model, Cmd Msg )
updateState fun ( regs, state, memory ) =
    ( ( regs, fun state, memory ), Cmd.none )


parseInstruction : Int -> Instruction
parseInstruction cellValue =
    if cellValue >= 100 && cellValue < 200 then
        Add <| Basics.modBy 100 cellValue

    else if cellValue >= 200 && cellValue < 300 then
        Subtract <| Basics.modBy 100 cellValue

    else if cellValue >= 300 && cellValue < 400 then
        Store <| Basics.modBy 100 cellValue

    else if cellValue >= 400 && cellValue < 500 then
        Load <| Basics.modBy 100 cellValue

    else if cellValue >= 500 && cellValue < 600 then
        Jump <| Basics.modBy 100 cellValue

    else if cellValue == 1 then
        Input

    else if cellValue == 2 then
        Output

    else if cellValue == 3 then
        BranchIfZero

    else if cellValue == 4 then
        BranchIfNegative

    else
        Halt


readInput : Input -> Maybe ( Int, Input )
readInput inp =
    case inp of
        [] ->
            Nothing

        "" :: rest ->
            readInput rest

        head :: rest ->
            String.toInt head
                |> andThen
                    (\val ->
                        if val >= 0 && val <= 999 then
                            Just ( val, rest )

                        else
                            Nothing
                    )


step : Model -> Instruction -> Model
step (( ( PC pc, Acc acc negative ) as regs, (State running inp outp) as state, memory ) as model) instruction =
    case instruction of
        Halt ->
            ( regs, State False inp outp, memory )

        Add cellNumber ->
            accFromCell (\a c -> a + c) cellNumber model

        Subtract cellNumber ->
            accFromCell (\a c -> a - c) cellNumber model

        Load cellNumber ->
            accFromCell (\_ c -> c) cellNumber model

        Store cellNumber ->
            ( ( PC <| Basics.modBy 100 (pc + 1), Acc acc negative ), state, List.Extra.setAt cellNumber acc memory )

        Jump cellNumber ->
            ( ( PC cellNumber, Acc acc negative ), state, memory )

        BranchIfZero ->
            let
                pcIncrement =
                    if acc == 0 then
                        2

                    else
                        1
            in
            ( ( PC <| Basics.modBy 100 (pc + pcIncrement), Acc acc negative ), state, memory )

        BranchIfNegative ->
            let
                pcIncrement =
                    if negative then
                        2

                    else
                        1
            in
            ( ( PC <| Basics.modBy 100 (pc + pcIncrement), Acc acc negative ), state, memory )

        Input ->
            case readInput inp of
                Just ( inputNumber, newInp ) ->
                    ( ( PC <| Basics.modBy 100 (pc + 1), Acc inputNumber False ), State running newInp outp, memory )

                Nothing ->
                    ( regs, State False inp outp, memory )

        Output ->
            ( ( PC <| Basics.modBy 100 (pc + 1), Acc acc negative ), State running inp (outp ++ [ acc ]), memory )


accFromCell : (Int -> Int -> Int) -> Int -> Model -> Model
accFromCell fun cellNumber (( ( PC pc, Acc acc _ ) as regs, (State running inp outp) as state, memory ) as model) =
    case List.Extra.getAt cellNumber memory of
        Just cellVal ->
            let
                newAccVal =
                    fun acc cellVal

                negative =
                    newAccVal < 0
            in
            ( ( PC <| Basics.modBy 100 (pc + 1), Acc (Basics.max 0 <| Basics.modBy 1000 <| newAccVal) negative ), state, memory )

        Nothing ->
            model



-- VIEW


toInt : Int -> String -> Int
toInt n v =
    Basics.max 0 <| Maybe.withDefault 0 <| String.toInt <| String.right n v


fromInt : Int -> Int -> String
fromInt n v =
    String.padLeft n '0' <| String.fromInt v


memoryCell : Int -> Int -> PC -> Html Msg
memoryCell cellNumber cellValue (PC pc) =
    let
        memoryValue =
            fromInt 3 cellValue

        memoryLabel =
            fromInt 2 cellNumber

        memoryId =
            "memory_" ++ memoryLabel
    in
    span
        ([ class "cell" ]
            ++ (if cellNumber == pc then
                    [ class "selected" ]

                else
                    []
               )
        )
        [ input
            [ onInput (\v -> SetCell cellNumber <| toInt 3 v)
            , value memoryValue
            , size 3
            , id memoryId
            ]
            []
        ]


memoryRows : List Int -> PC -> List (Html Msg)
memoryRows cells pc =
    [ div [ class "memrow" ] <|
        span
            [ class "col-header" ]
            []
            :: map
                (\i -> span [ class "col-header" ] [ text <| String.fromInt i ])
                (range 0 9)
    ]
        ++ List.indexedMap
            (\i row ->
                div [ class "memrow" ] <|
                    [ span [ class "row-header" ] [ text <| String.fromInt (10 * i) ] ]
                        ++ row
            )
            (greedyGroupsOf 10 <|
                List.indexedMap (\i v -> memoryCell i v pc) cells
            )


ioView : State -> List (Html Msg)
ioView (State _ inputs output) =
    [ div [ class "col-sm" ]
        [ Html.h3 []
            [ label [ for "input" ]
                [ text "Input" ]
            ]
        , input
            [ id "input"
            , onInput (\v -> SetInput v)
            , value <| String.join " " inputs
            ]
            []
        ]
    , div [ class "col-sm" ]
        [ Html.h3 []
            [ label [ for "output" ]
                [ text "Output" ]
            ]
        , div [ id "output" ]
            (map (\v -> Html.p [] [ text (String.fromInt v) ]) output)
        ]
    ]


view : Model -> Browser.Document Msg
view ( ( PC pc, Acc acc flagNegative ), state, memory ) =
    Browser.Document
        "LMC"
    <|
        [ div [ class "container" ]
            [ Html.h1 [] [ text "Little Man Computer" ]
            , div
                [ class "registers"
                , class "row justify-content-start"
                ]
                [ div [ class "col-sm" ]
                    [ label [ for "reg_pc", title "Program Counter" ] [ text "Next instruction from cell" ]
                    , input
                        [ id "reg_pc"
                        , value <| fromInt 2 pc
                        , title "Program Counter"
                        , onInput (\v -> SetPC <| toInt 2 v)
                        ]
                        []
                    ]
                , div [ class "col-sm" ]
                    [ label [ for "reg_acc", title "Accumulator" ] [ text "Accumulator" ]
                    , input
                        [ id "reg_acc"
                        , value <| fromInt 3 acc
                        , title "Accumulator"
                        , onInput (\v -> SetAcc <| toInt 3 v)
                        ]
                        []
                    , span [ class "flags", class "px-2" ]
                        (if flagNegative then
                            [ span [ id "flag_negative", class "px-1 bg-warning", title "Last operation result was negative" ] [ text "N" ] ]

                         else
                            []
                        )
                    ]
                ]
            , div [ class "controls", class "row" ]
                [ div [ class "col-sm" ]
                    [ button
                        [ onClick Step
                        ]
                        [ text "Step" ]
                    ]
                , div [ class "col-sm" ]
                    [ button
                        [ onClick ClearOutput
                        ]
                        [ text "Clear Output" ]
                    ]
                , div [ class "col-sm" ]
                    [ button
                        [ onClick Start
                        ]
                        [ text "Start" ]
                    ]
                , div [ class "col-sm" ]
                    [ button
                        [ onClick Stop
                        ]
                        [ text "Stop" ]
                    ]
                ]
            , div [ class "row  p-3" ]
                [ div
                    [ class "col" ]
                    [ div
                        [ class "memory" ]
                        (memoryRows memory (PC pc))
                    , div [ class "row" ] <| ioView state
                    ]
                , div [ class "col" ]
                    [ Html.h2 [] [ text "Instruction set" ]
                    , Html.dl []
                        [ Html.dt [] [ text "1xx ADD" ]
                        , Html.dd [] [ text "Add number from cell xx to Accumulator" ]
                        , Html.dt [] [ text "2xx SUB" ]
                        , Html.dd [] [ text "Subtract number from cell xx from Accumulator" ]
                        , Html.dt [] [ text "3xx STOR" ]
                        , Html.dd [] [ text "Store number from Accumulator to cell xx" ]
                        , Html.dt [] [ text "4xx LOAD" ]
                        , Html.dd [] [ text "Load number from cell xx to Accumulator" ]
                        , Html.dt [] [ text "5xx JMP" ]
                        , Html.dd [] [ text "Jump to cell xx" ]
                        , Html.dt [] [ text "000 HLT" ]
                        , Html.dd [] [ text "Ends the program" ]
                        , Html.dt [] [ text "001 INP" ]
                        , Html.dd [] [ text "Read number from input to Accumulator" ]
                        , Html.dt [] [ text "002 OUT" ]
                        , Html.dd [] [ text "Write number from Accumulator to output" ]
                        , Html.dt [] [ text "003 BZ" ]
                        , Html.dd [] [ text "If Accumulator is zero, skip next cell" ]
                        , Html.dt [] [ text "004 BNEG" ]
                        , Html.dd [] [ text "If last operation was negative, skip next cell" ]
                        ]
                    ]
                ]
            ]
        ]
